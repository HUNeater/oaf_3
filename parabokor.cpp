//
// Created by zsolt on 11/17/17.
//

#include "parabokor.h"

int Parabokor::react(Radiation *r) {
    switch (r->type) {
        case RadiationType::Delta:
        case RadiationType::Alpha:
            nutrients++;
            break;
        case RadiationType::Neutral:
            nutrients--;
            break;
    }

    if (nutrients <= 0)
        bAlive = false;

    return 0;
}