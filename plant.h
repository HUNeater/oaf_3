//
// Created by zsolt on 11/17/17.
//

#ifndef OAF_3_PLANT_H
#define OAF_3_PLANT_H

#include <string>
#include <utility>
#include "Radiation.h"

class Plant {
protected:
    int nutrients;
    std::string name;
    bool bAlive;

    Plant(std::string name, int nutrients) : name(std::move(name)), nutrients(nutrients) {
        bAlive = nutrients > 0;
    }

public:
    void print();

    bool isAlive() { return bAlive; }

    int getNutrients() { return nutrients; }

    virtual int react(Radiation *) = 0;

    std::string getName() { return name; }
};


#endif //OAF_3_PLANT_H
