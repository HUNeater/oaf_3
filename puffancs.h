//
// Created by zsolt on 11/17/17.
//

#ifndef OAF_3_PUFFANCS_H
#define OAF_3_PUFFANCS_H


#include "plant.h"

class Puffancs : public Plant {
public:
    Puffancs(const std::string &name, int nutrients) : Plant(name, nutrients) {
        bAlive = nutrients > 0 && nutrients <= 10;
    }

private:
    int react(Radiation*) override;
};


#endif //OAF_3_PUFFANCS_H
