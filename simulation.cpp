//
// Created by zsolt on 11/17/17.
//

#include <memory>
#include <iostream>
#include <fstream>
#include "plant.h"
#include "simulation.h"
#include "puffancs.h"
#include "deltafa.h"
#include "parabokor.h"

void Simulation::simNextDay() {
    currentDay++;

    if (currentDay > days)
        throw Exceptions::SIM_OVER;

    std::cout << "After day " << currentDay << ":" << std::endl;

    Radiation *r;
    if (abs(alphaRad->requests - deltaRad->requests) < 3) {
        r = neutralRad;
    } else if (alphaRad->requests - 3 > deltaRad->requests) {
        r = alphaRad;
    } else {
        r = deltaRad;
    }

    std::cout << "Radiation: ";
    switch (r->type) {
        case RadiationType::Alpha:
            std::cout << "Alpha" << std::endl;
            break;
        case RadiationType::Delta:
            std::cout << "Delta" << std::endl;
            break;
        case RadiationType::Neutral:
            std::cout << "Neutral" << std::endl;
            break;
    }

    for (auto &p : plants) {
        if (p->isAlive()) {
            int n = p->react(r);

            p->print();

            if (n < 0) {
                deltaRad->requests -= n;
            } else if (n > 0) {
                alphaRad->requests += n;
            }
        }
    }

    if (currentDay >= 1 && currentDay < days)
        std::cout << std::endl << "------------------------------------------" << std::endl << std::endl;
}

void Simulation::simAll() {
    while (currentDay < days) {
        simNextDay();
    }
}

void Simulation::addPlant(Plant *p) {
    std::shared_ptr < Plant > sp(p);

    plants.push_back(sp);
}

Simulation::~Simulation() {
    delete alphaRad;
    delete deltaRad;
    delete neutralRad;
}

Simulation::Simulation(const char *path) {
    std::ifstream file(path);
    unsigned int nPlant, nutrients, d;
    std::string type, name;

    file >> nPlant;

    for (unsigned int i = 0; i < nPlant; ++i) {
        file >> name >> type >> nutrients;

        Plant *p;
        if (type == "a") {
            p = new Puffancs(name, nutrients);
        } else if (type == "d") {
            p = new DeltaFa(name, nutrients);
        } else {
            p = new Parabokor(name, nutrients);
        }

        addPlant(p);
    }

    file >> d;
    days = d;
}
