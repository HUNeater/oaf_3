//
// Created by zsolt on 11/17/17.
//

#ifndef OAF_3_RADIATION_H
#define OAF_3_RADIATION_H

enum class RadiationType {
    Alpha, Delta, Neutral
};

class Radiation {
public:
    explicit Radiation(RadiationType t) : type(t) {};
    RadiationType type;
    int requests = 0;
};


#endif //OAF_3_RADIATION_H
