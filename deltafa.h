//
// Created by zsolt on 11/17/17.
//

#ifndef OAF_3_DELTAFA_H
#define OAF_3_DELTAFA_H


#include "plant.h"

class DeltaFa : public Plant {
public:
    DeltaFa(const std::string &name, int nutrients) : Plant(name, nutrients) {}

private:
    int react(Radiation*) override;
};


#endif //OAF_3_DELTAFA_H
