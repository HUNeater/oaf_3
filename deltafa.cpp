//
// Created by zsolt on 11/17/17.
//

#include "deltafa.h"

int DeltaFa::react(Radiation *r) {
    switch (r->type) {
        case RadiationType::Alpha:
            nutrients -= 3;
            break;
        case RadiationType::Delta:
            nutrients += 4;
            break;
        case RadiationType::Neutral:
            nutrients--;
            break;
    }

    if (nutrients < 0)
        bAlive = false;

    if (nutrients < 5) {
        return -4;
    } else if (nutrients > 5 && nutrients < 10) {
        return -1;
    }

    return 0;
}