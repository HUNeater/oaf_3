//
// Created by zsolt on 11/17/17.
//

#include <iostream>
#include "plant.h"

void Plant::print() {
    std::cout << "- " << getName() << ":\t nutrients=" << nutrients << "; \t bAlive=" << isAlive() << std::endl;
}