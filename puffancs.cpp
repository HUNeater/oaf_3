//
// Created by zsolt on 11/17/17.
//

#include "puffancs.h"

int Puffancs::react(Radiation *r) {
    switch (r->type) {
        case RadiationType::Alpha:
            nutrients += 2;
            break;
        case RadiationType::Delta:
            nutrients -= 2;
            break;
        case RadiationType::Neutral:
            nutrients--;
            break;
    }

    if (nutrients <= 0 || nutrients > 10)
        bAlive = false;

    return abs(10 - nutrients);
}
