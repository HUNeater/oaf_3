//
// Created by zsolt on 11/17/17.
//

#ifndef OAF_3_PARABOKOR_H
#define OAF_3_PARABOKOR_H


#include "plant.h"

class Parabokor : public Plant {
public:
    Parabokor(const std::string &name, int nutrients) : Plant(name, nutrients) {}

private:
    int react(Radiation *) override;
};


#endif //OAF_3_PARABOKOR_H
