#include <iostream>
#include "deltafa.h"
#include "parabokor.h"
#include "puffancs.h"
#include "simulation.h"

int main() {

    Simulation sim("in1.txt");

//    sim.addPlant(new DeltaFa("DeltaFa 1", 5));
//    sim.addPlant(new Puffancs("Puffancs 1", 10));
//    sim.addPlant(new DeltaFa("DeltaFa 2", 15));
//    sim.addPlant(new Puffancs("Puffancs 2", 20));
//    sim.addPlant(new Parabokor("Parabokor 1", 25));
//    sim.addPlant(new Parabokor("Parabokor 3", 1));
//    sim.addPlant(new Parabokor("Parabokor 4", 5));

    sim.simAll();

    return 0;
}