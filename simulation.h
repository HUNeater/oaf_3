//
// Created by zsolt on 11/17/17.
//

#ifndef OAF_3_SIMULATION_H
#define OAF_3_SIMULATION_H

#include <deque>
#include <memory>
#include "plant.h"
#include "Radiation.h"

enum class Exceptions {
    SIM_OVER
};

class Simulation {
    std::deque<std::shared_ptr<Plant>> plants;
    Radiation *deltaRad = new Radiation(RadiationType::Delta);
    Radiation *alphaRad = new Radiation(RadiationType::Alpha);
    Radiation *neutralRad = new Radiation(RadiationType::Neutral);
    unsigned int currentDay = 0;
    unsigned int days;

public:
    explicit Simulation(unsigned int d) : days(d) {};

    explicit Simulation(const char *);

    ~Simulation();

    void addPlant(Plant *p);

    void simNextDay();

    void simAll();

    Plant *getPlant(unsigned int i) { return plants.at(i).get(); }
};


#endif //OAF_3_SIMULATION_H
