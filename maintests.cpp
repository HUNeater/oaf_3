//
// Created by zsolt on 11/17/17.
//

#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "simulation.h"
#include "deltafa.h"
#include "parabokor.h"
#include "puffancs.h"

TEST_CASE("General") {
    SECTION("Puffancs") {
        Simulation sim(5);
        sim.addPlant(new Puffancs("p", 5));

        sim.simNextDay();
        CHECK(sim.getPlant(0)->isAlive());
        CHECK(sim.getPlant(0)->getNutrients() == 4);

        sim.simNextDay();
        CHECK(sim.getPlant(0)->isAlive());
        CHECK(sim.getPlant(0)->getNutrients() == 6);

        sim.simNextDay();
        CHECK(sim.getPlant(0)->isAlive());
        CHECK(sim.getPlant(0)->getNutrients() == 8);

        sim.simNextDay();
        CHECK(sim.getPlant(0)->isAlive());
        CHECK(sim.getPlant(0)->getNutrients() == 10);

        sim.simNextDay();
        CHECK_FALSE(sim.getPlant(0)->isAlive());
        CHECK(sim.getPlant(0)->getNutrients() == 12);
    }

    SECTION("DeltaFa") {
        Simulation sim(5);
        sim.addPlant(new DeltaFa("d", 5));

        sim.simNextDay();
        CHECK(sim.getPlant(0)->isAlive());
        CHECK(sim.getPlant(0)->getNutrients() == 4);

        sim.simNextDay();
        CHECK(sim.getPlant(0)->isAlive());
        CHECK(sim.getPlant(0)->getNutrients() == 8);

        sim.simNextDay();
        CHECK(sim.getPlant(0)->isAlive());
        CHECK(sim.getPlant(0)->getNutrients() == 12);

        sim.simNextDay();
        CHECK(sim.getPlant(0)->isAlive());
        CHECK(sim.getPlant(0)->getNutrients() == 16);

        sim.simNextDay();
        CHECK(sim.getPlant(0)->isAlive());
        CHECK(sim.getPlant(0)->getNutrients() == 20);
    }

    SECTION("DeltaFa") {
        Simulation sim(5);
        sim.addPlant(new Parabokor("p", 5));

        sim.simNextDay();
        CHECK(sim.getPlant(0)->isAlive());
        CHECK(sim.getPlant(0)->getNutrients() == 4);

        sim.simNextDay();
        CHECK(sim.getPlant(0)->isAlive());
        CHECK(sim.getPlant(0)->getNutrients() == 3);

        sim.simNextDay();
        CHECK(sim.getPlant(0)->isAlive());
        CHECK(sim.getPlant(0)->getNutrients() == 2);

        sim.simNextDay();
        CHECK(sim.getPlant(0)->isAlive());
        CHECK(sim.getPlant(0)->getNutrients() == 1);

        sim.simNextDay();
        CHECK_FALSE(sim.getPlant(0)->isAlive());
        CHECK(sim.getPlant(0)->getNutrients() == 0);
    }
}

TEST_CASE("From file") {
    Simulation sim("in1.txt");
    sim.simAll();
    
    CHECK(sim.getPlant(0)->isAlive());
    CHECK_FALSE(sim.getPlant(1)->isAlive());
    CHECK(sim.getPlant(2)->isAlive());
    CHECK_FALSE(sim.getPlant(3)->isAlive());
}